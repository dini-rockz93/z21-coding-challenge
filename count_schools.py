from schools import Schools

result = Schools()

# Driver code
result.get_total_schools()
result.get_schools_by_state()
result.get_schools_by_metro()
result.get_city_most_schools()
result.get_city_min_one_school()
result.search_schools('elementary')
result.search_schools('jefferson belleville')
result.search_schools('riverside school 44')
result.search_schools('KUSKOKWIM')
