# NuffSaid Backend Programming Project 
- https://gitlab.com/hari22/nuffsaid-coding-challenge

### count_schools.py -> contains the driver code
### schools.py -> contains the class for the implementation of the project
### config.py -> contains the neccessary config file for the class School
### To run the code, run count_schools.py -> python -u "./count_schools.py"