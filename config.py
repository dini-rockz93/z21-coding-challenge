file = "./school_data.csv"
STATE = 5
MLOCALE = 8
CITY = 4
SCHOOL_ID = 0
initval = 0
SCHOOL_NAME = 3
header_start = "NCESSCH"
increment_one = 1
comma = ','
SCHOOL = "SCHOOL"
empty_string = ''
seconds = 1000
messages = {

    "schools": "<SCHOOLS> {}",
    "total_schools": "Total schools: {}",
    "search_output": "Results: {}",
    "time_results": "Results for {} (search took: {}ms)",
    "schools_state": "Schools by state: {}",
    "schools_metro": "Schools by metro locale: {}",
    "schools_max_city": "City with most schools: {}", 
    "unique": "Unique cities with at least one school: {}",

}
