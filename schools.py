from csv import reader
from time import time
from config import file, STATE, MLOCALE, CITY, SCHOOL_ID, initval, header_start, increment_one, comma, messages, seconds, empty_string, SCHOOL

class Schools():

	def __init__(self):

		self.curr_state = empty_string
		self.curr_city = empty_string
		self.results = list()
		self.score_location = initval
		self.score_school = initval

		self.SCHOOLS = dict()
		self.school_count = initval
		self.school_count_by_state = dict()
		self.school_count_by_metro = dict()
		self.school_count_by_city = dict()

		with open(file) as csv_file:

			csv_reader = reader(csv_file, delimiter=comma)

			for row in csv_reader:

				#skip the header
				if row[initval] == header_start:
					continue
				
				if row[STATE] not in self.SCHOOLS:
					self.SCHOOLS[row[STATE]] = dict()
					self.school_count_by_state[row[STATE]] = initval
 					
				if row[MLOCALE] not in self.SCHOOLS[row[STATE]]:
					self.SCHOOLS[row[STATE]][row[MLOCALE]] = dict()
					self.school_count_by_metro[row[MLOCALE]] = initval

				if row[CITY] not in self.SCHOOLS[row[STATE]][row[MLOCALE]]:
					self.SCHOOLS[row[STATE]][row[MLOCALE]][row[CITY]] = list()
					self.school_count_by_city[row[CITY]] = initval				

				self.SCHOOLS[row[STATE]][row[MLOCALE]][row[CITY]].append(row[SCHOOL_ID])

				self.school_count += increment_one
				self.school_count_by_state[row[STATE]] += increment_one
				self.school_count_by_metro[row[MLOCALE]] += increment_one
				self.school_count_by_city[row[CITY]] += increment_one


	def __str__(self):
		return messages["schools"].format(self.SCHOOLS)
		
	def get_total_schools(self):
		print(messages["total_schools"].format(self.school_count))

	def get_schools_by_state(self):
		print(messages["schools_state"].format('\n'.join(['{}: {}'.format(index, self.school_count_by_state[index]) for index in sorted(self.school_count_by_state)])))

	def get_schools_by_metro(self):
		print(messages["schools_metro"].format('\n'.join(['{}: {}'.format(index, self.school_count_by_metro[index]) for index in sorted(self.school_count_by_metro)])))

	def get_city_most_schools(self):
		print(messages["schools_max_city"].format(sorted(self.school_count_by_city.items(), key=lambda kv: kv[increment_one])[len(self.school_count_by_city)-increment_one]))

	def get_city_min_one_school(self):
		print(messages["unique"].format(len(self.school_count_by_city)))


	def search_schools(self,query):
    
		start_ms = int(round(time() * seconds))

		def extract(node,search_string):
			"""
			Hash structure: state => city => school
			---------------
			Location search
			---------------
			"""
			if isinstance(node,dict):

				for k,v in node.items():

					if len(k) == 2:
						self.curr_state = k
					else:
						self.curr_city = k

					self.score_location = initval

					for i in [x.strip().upper() for x in search_string.split(' ')]:
						if i in k:

							self.score_location += increment_one 
			
					if isinstance(node, dict):
						extract(v,search_string)
			
			elif isinstance(node,list): 

				self.score_school = initval
				for school_name in node:
					
					for i in [x.strip().upper() for x in search_string.split(' ')]:
						
						if i == SCHOOL :
							continue
						if i in school_name and i not in self.curr_city and i not in self.curr_state:
							self.score_school += 5
						else:
							self.score_school -= 1

					if self.score_school > initval or self.score_location > initval:
						total_score = self.score_school + self.score_location
						self.results.append((total_score,'{} => {}, {}'.format(school_name,self.curr_city,self.curr_state)))

					self.score_school = initval 

		extract(self.SCHOOLS, query)
		
		stop_ms = int(round(time() * seconds)) 
		
		results = sorted(self.results)[-3:] 
		print(messages["search_output"].format('\n'.join([x[1] for x in results[::-1]])))
		self.results = list()
		
		print(messages["time_results"].format(query, stop_ms - start_ms))
		
